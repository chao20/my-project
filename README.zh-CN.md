<div align="center">
  <h1>V3 Admin Plus</h1>
</div>

## 简介

基于 Vue3、TypeScript、Element Plus、Pinia 和 Vite 等主流技术的一个中后台管理系统，主要目的是学习以及工作使用

<!-- - Vue-Cli 5.x 版: [v3-admin-elementPlus](https://github.com/un-pany/v3-admin) -->

## 环境要求

- Node 环境

  版本：16+

- 开发工具

  VSCode

- 必装插件

  - Vue Language Features (Volar)
  - TypeScript Vue Plugin (Volar)

## 项目预览图

![preview1.png](./src/assets/docs/preview1.png)
![preview2.png](./src/assets/docs/preview2.png)
![preview3.png](./src/assets/docs/preview3.png)
![preview4.png](./src/assets/docs/preview4.png)

## 特性

- **Vue3**：采用 Vue3 + script setup 最新的 Vue3 组合式 API
- **Element Plus**：Element UI 的 Vue3 版本
- **Pinia**: Vuex5（好用）
- **Vite**：真的很快
- **Vue Router**：路由路由
- **TypeScript**：JavaScript 语言的超集
- **PNPM**：更快速的，节省磁盘空间的包管理工具
- **Scss**：和 Element Plus 保持一致
- **CSS 变量**：主要控制项目的布局和颜色
- **ESlint**：代码校验
- **Prettier**：代码格式化
- **Axios**：发送网络请求（已封装好）
- **UnoCSS**：具有高性能且极具灵活性的即时原子化 CSS 引擎
- **注释**：各个配置项都写有尽可能详细的注释
- **兼容移动端**: 布局兼容移动端页面分辨率

## 功能

- **用户管理**：登录、登出
- **权限管理**：内置页面权限（动态路由）、指令权限、权限函数、路由守卫
- **多环境**：开发环境（development）、预发布环境（staging）、正式环境（production）
- **多主题**：内置普通、黑暗、深蓝三种主题模式
- **菜单位置**：支持菜单栏左侧和顶部切换
- **菜单背景**：无用的功能 添加了一个切换菜单背景功能（😂）
- **支持国际化**：集成 i18n（国内项目可自行去掉）
- **错误页面**: 403、404
- **Dashboard**：首页看板
- **其他内置功能**：SVG、动态侧边栏、动态面包屑、标签页快捷导航、Screenfull 全屏、自适应收缩侧边栏

## 国内仓库

[Gitee](https://gitee.com/chao20/my-project)

## 在线预览

[vue3-admin-plus](https://chao20.gitee.io/my-project)

启动项目后 localhost:3030  
后续将增加在线预览链接

<!-- | 位置         | 账号            | 链接                                            |
| ------------ | --------------- | ----------------------------------------------- |
| github-pages | admin 或 editor | [链接](https://un-pany.github.io/v3-admin-vite) | -->

## 🛠 开发

```bash

# 克隆项目
git clone https://gitee.com/chao20/my-project.git

# 进入项目目录
cd my-project

# 安装依赖
pnpm i

# 启动服务
pnpm dev
```

## ✔️ 预览

```bash
# 预览预发布环境
pnpm preview:stage

# 预览正式环境
pnpm preview:prod
```

## 📦️ 多环境打包

```bash
# 构建预发布环境
pnpm build:stage

# 构建正式环境
pnpm build:prod
```

## 🔧 代码检查

```bash
# 代码格式化
pnpm lint

# 单元测试
pnpm test
```

## 感谢 Star

免费开源项目，如果你喜欢这个项目的话，欢迎支持一个 star ！

## 有什么问题加 QQ 或者微信讨论

QQ ：171853328（左）&& 加我微信（右）

![qq.jpg](./src/assets/docs/qq.jpg)
![wechat.jpg](./src/assets/docs/wechat.jpg)

## 📄 License

[MIT](./LICENSE)

Copyright (c) 2023 [pany](https://gitee.com/chao20/my-project.git)
