import { defineStore } from "pinia"
import store from "@/store"
import i18n from "@/lang"

const useLangStore = defineStore("lang", {
  state: () => {
    return {
      locale: localStorage.getItem("lang") || "zhCn"
    }
  },
  actions: {
    SET_LOCALE(locale: any) {
      //语言切换
      this.locale = locale
      localStorage.setItem("lang", locale)
      i18n.global.locale.value = locale
    }
  },
  persist: {
    enabled: true
  }
})

export function useAppStoreHook() {
  return useLangStore(store)
}
