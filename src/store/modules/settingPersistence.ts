// 新增部分setting 持久内容
import { defineStore } from "pinia"
import layoutSettings from "@/config/layout"
export const settingPersistence = defineStore({
  id: "settingPersistence",
  state: () => {
    return {
      showBgImage: layoutSettings.showBgImage || "1",
      layout: layoutSettings.layout || "left"
    }
  },
  // 开启数据缓存
  persist: {
    enabled: true,
    strategies: [
      {
        storage: sessionStorage,
        paths: ["showBgImage", "layout"] //指定要长久化的字段 学习使用  正式开发可去掉
      }
    ]
  }
})
