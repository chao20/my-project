export interface IListItem {
  avatar?: string
  title: string
  datetime?: string
  description?: string
  status?: "" | "success" | "info" | "warning" | "danger"
  extra?: string
}

export const notifyData: IListItem[] = [
  {
    avatar: "https://img0.baidu.com/it/u=353479326,2613865378&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500",
    title: "上线啦",
    datetime: "2023-05-20",
    description: "基于 Vue3、TypeScript、Element Plus、Pinia 和 Vite 等主流技术"
  },
  {
    avatar: "https://img0.baidu.com/it/u=353479326,2613865378&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500",
    title: "admin-plus 上线啦",
    datetime: "2023-05-20",
    description: "中后台管理系统，基于 Vue3、TypeScript、Element Plus 和 Pinia"
  }
]

export const messageData: IListItem[] = [
  {
    avatar: "https://gw.alipayobjects.com/zos/rmsportal/ThXAXghbEsBCCSDihZxY.png",
    title: "肖申克的救赎",
    description: "在这世上，有些东西是石头无法刻成的，在我们心里，有一块地方是无法锁住的，那块地方叫做希望。",
    datetime: "2023-05-20"
  },
  {
    avatar: "https://gw.alipayobjects.com/zos/rmsportal/ThXAXghbEsBCCSDihZxY.png",
    title: "来自大话西游",
    description: "如果非要在这份爱上加上一个期限，我希望是一万年",
    datetime: "2023-05-20"
  }
]

export const todoData: IListItem[] = [
  {
    title: "任务名称",
    description: "这家伙很懒，什么都没留下",
    extra: "未开始",
    status: "info"
  },
  {
    title: "任务名称",
    description: "这家伙很懒，什么都没留下",
    extra: "进行中",
    status: ""
  },
  {
    title: "任务名称",
    description: "这家伙很懒，什么都没留下",
    extra: "已超时",
    status: "danger"
  }
]
