import { watch, onMounted, onBeforeUnmount } from "vue"
import { storeToRefs } from "pinia"
import { useRoute } from "vue-router"
import { useAppStore, DeviceType } from "@/store/modules/app"
import { settingPersistence } from "@/store/modules/settingPersistence"

/** 参考 Bootstrap 的响应式设计 WIDTH = 992 */
const WIDTH = 992

/** 根据大小变化重新布局 */
export default () => {
  const route = useRoute()
  const appStore = useAppStore()
  const { layout } = storeToRefs(settingPersistence())

  const _isMobile = () => {
    // getBoundingClientRect() 方法返回元素的大小及其相对于视口的位置
    const rect = document.body.getBoundingClientRect()
    return rect.width - 1 < WIDTH
  }

  const _resizeHandler = () => {
    if (!document.hidden) {
      const isMobile = _isMobile()
      appStore.toggleDevice(isMobile ? DeviceType.Mobile : DeviceType.Desktop)
      if (isMobile) {
        appStore.closeSidebar(true)
      }
    }
  }

  watch(
    () => route.name,
    () => {
      if (appStore.device === DeviceType.Mobile && appStore.sidebar.opened) {
        appStore.closeSidebar(false)
      }
    }
  )
  // 监听layout 位置 top  left 目前是这两种   是否需要监听resize
  watch(
    layout,
    (newV) => {
      if (newV === "left") {
        window.addEventListener("resize", _resizeHandler)
      } else {
        window.removeEventListener("resize", _resizeHandler)
      }
    },
    {
      immediate: true
    }
  )

  // onBeforeMount(() => {
  //   window.addEventListener("resize", _resizeHandler)
  // })

  onMounted(() => {
    if (_isMobile()) {
      appStore.toggleDevice(DeviceType.Mobile)
      appStore.closeSidebar(true)
    }
  })

  onBeforeUnmount(() => {
    window.removeEventListener("resize", _resizeHandler)
  })
}
