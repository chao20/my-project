import { createI18n } from "vue-i18n"
//  本地国际化资源
import zhCn from "./package/zh-cn"
import en from "./package/en"

// 创建 i18n
const i18n = createI18n({
  legacy: false,
  globalInjection: true, // 全局模式，可以直接使用 $t
  locale: localStorage.getItem("lang") || "zhCn",
  messages: {
    zhCn,
    en
  }
})

export default i18n
